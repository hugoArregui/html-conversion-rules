(module html-conversion-rules
        *
        (import chicken scheme)
        (use matchable data-structures sxml-transforms matchable srfi-1 srfi-13 doctype)

(define (unpack-elem e f)
  (match e
         ((tag ('@ . attrs) . body)
          (f tag attrs body))
         ((tag . body)
          (f tag '() body))))

(define-syntax unpack-rule
  (syntax-rules ()
    ((unpack-rule args body ...)
     (lambda (t b)
       (unpack-elem (cons t b) (lambda args
                                 body ...))))))


(define (make-elem tag attrs body)
  (cons* tag (if (null? attrs)
               '()
               `(@ ,@attrs)) body))

(define (add-class class elem)
  (unpack-elem elem
               (lambda (tag attrs body)
                 (make-elem tag
                            (let* ((class-attr (assq 'class attrs))
                                   (classes    (and class-attr
                                                    (cadr class-attr))))
                              (cond ((not class-attr)
                                     (append attrs `((class ,class))))
                                    ((member class (string-split classes))
                                     attrs)
                                    (#t
                                     (set-cdr! class-attr `(,(string-append class " " classes)))
                                     attrs)))
                            body))))

(define (remove-class class elem)
  (unpack-elem elem
               (lambda (tag attrs body)
                 (make-elem tag (map (lambda (attr)
                                       (if (eq? 'class (car attr))
                                         (let* ((classes (string-split (cadr attr)))
                                                (new-classes (string-join
                                                               (filter (lambda (c)
                                                                         (not (string=? c class))) classes))))
                                           (if (string=? new-classes "")
                                             '()
                                             `(class ,new-classes)))
                                         attr))
                                     attrs) body))))

(define html-rules
  `((css-link *macro* . ,(lambda (t b)
                           (match b
                                  ((href)
                                   `(link (@ (href ,href) (type "text/css") (rel "stylesheet")))))))
    (js-link  *macro* . ,(lambda (t b)
                           (match b
                                  ((src)
                                   `(script (@ (type "text/js") (src ,src)))))))
    (url      *macro* . ,(unpack-rule (tag attrs body)
                           (match body
                                  ((href . body)
                                   `(a (@ ,@(append attrs `((href ,href)))) ,@body)))))
    (itemize  *macro* . ,(unpack-rule (tag attrs body)
                           (make-elem 'ul attrs (map (lambda (e) `(li ,e)) body))))))

(define (bootstrap-rules #!optional (prefix '||))
  (let ((make-macro-rule (lambda (name rule)
                           `(,(if (eq? '|| prefix)
                                name
                                (symbol-append prefix ': name)) *macro* . ,rule))))
    (list
      (make-macro-rule 'row
                       (lambda (t b)
                         (add-class "row" `(div ,@b))))
      (make-macro-rule 'container
                       (lambda (t b)
                         (add-class "container" `(div ,@b))))
      (make-macro-rule 'page-header
                       (lambda (t b)
                         (add-class "page-header" `(div ,@b))))
      (make-macro-rule 'text
                       (unpack-rule (tag attrs body)
                         `(input
                            (@ (type "text") (class "form-control")
                               ,(if (null? body) '() `(value ,(car body)))
                               ,@attrs))))
      (make-macro-rule 'input-group
                       (lambda (t b)
                         (add-class "input-group" `(div ,@b))))
      (make-macro-rule 'button
                       (unpack-rule (tag attrs body)
                         (let-values (((class b)
                                       (if (member (car body)
                                                   '(default primary success info warning danger link))
                                         (values (symbol->string (car body)) (cdr body))
                                         (values "default" body))))
                           `(button
                              (@ (class ,(string-append "btn btn-" class)) (type "button") . ,attrs) ,@b))))
      (make-macro-rule 'simple-panel
                       (lambda (t b)
                         `(div (@ (class "panel panel-default"))
                               (div (@ (class "panel-body"))
                                    ,@b))))
      (make-macro-rule 'icon
                       (lambda (t b)
                         `(span (@ (class ,(string-append "glyphicon " (car b))) (aria-hidden "true"))))))))

(define (rules #!key
               (include-bootstrap-rules #t) (bootstrap-rules-prefix '||)
               (include-html-rules #t)
               (include-doctype-rules #t))
  (append (if bootstrap-rules
            (bootstrap-rules bootstrap-rules-prefix)
            '())
          (if include-html-rules
            html-rules
            '())
          (if include-doctype-rules
            doctype-rules
            '())
          universal-conversion-rules*))


) ; module end

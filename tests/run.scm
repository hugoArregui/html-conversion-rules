(use html-conversion-rules sxml-transforms test srfi-1 matchable symbol-utils)

(define elem=?
  (let* ((sort-attrs  (lambda (attrs)
                        (sort attrs (lambda (attr1 attr2)
                                      (symbol-printname<? (car attr1) (car attr2))))))
         (filter-null (lambda (l)
                        (filter (o not null?) l)))
         (attrs=?     (lambda (attrs1 attrs2)
                        (list= equal?
                               (sort-attrs (filter-null attrs1))
                               (sort-attrs (filter-null attrs2)))))
         (body=?      (lambda (b1 b2)
                        (list= elem=? (filter-null b1) (filter-null b2)))))
    (lambda (e1 e2)
      (if (and (list? e1)
               (list? e2))
        (unpack-elem e1 (lambda (tag1 attrs1 body1)
                          (unpack-elem e2 (lambda (tag2 attrs2 body2)
                                            (and (eq? tag1 tag2)
                                                 (attrs=? attrs1 attrs2)
                                                 (body=? body1 body2))))))
        (equal? e1 e2)))))

(current-test-comparator elem=?)

(test-group "elem=?" ; test this crazy thing first, just in case
  (test "check simple eq"
        '(a (@ (class "button") (href "a")))
        '(a (@ (class "button") (href "a"))))

  (test "check eq of elements with unsorted attrs"
        '(a (@ (class "button") (href "a")))
        '(a (@ (href "a") (class "button"))))

  (test "check eq of elements with null attr"
        '(a chicken)
        '(a (@ ()) chicken))

  (test "check eq of elements with null attr (the other side)"
        '(a (@ ()) chicken)
        '(a chicken))

  (test-assert "check eq of not-eq elements"
               (not (elem=?
                      '(a (@ (class "button") (href "a")))
                      '(a (@ (href "a")))))))

(test-group "add-class"
  (test "add class to an element without args"
        '(a (@ (class "button")) "chicken")
        (add-class "button"
                   '(a "chicken")))
  (test "add class to an element with args"
        '(a (@ (class "button") (href "http://api.call-cc.org/")) "chicken")
        (add-class "button"
                   '(a (@ (href "http://api.call-cc.org/")) "chicken")))
  (test "add class to an element that already have a class defined"
        '(a (@ (class "button other") (href "http://api.call-cc.org/")) "chicken")
        (add-class "button"
                   '(a (@ (class "other") (href "http://api.call-cc.org/")) "chicken")))
  (test "add class to an element that already have *the* class defined"
        '(a (@ (class "button other") (href "http://api.call-cc.org/")) "chicken")
        (add-class "button"
                   '(a (@ (class "button other") (href "http://api.call-cc.org/")) "chicken"))))

(test-group "remove-class"
  (test "remove class from an element without args"
        '(a "chicken")
        (remove-class "button" '(a "chicken")))
  (test "remove class from an element with args, but without class"
        '(a (@ (href "http://api.call-cc.org/")) "chicken")
        (remove-class "button"
                      '(a (@ (href "http://api.call-cc.org/")) "chicken")))
  (test "remove class from an element with *the* class (and others)"
        '(a (@ (class "other")) "chicken")
        (remove-class "button"
                      '(a (@ (class "other button")) "chicken")))
  (test "remove class from an element with only *the* class"
        '(a "chicken")
        (remove-class "button"
                      '(a (@ (class "button")) "chicken"))))

(test-group "html-rules"
  (let ((basic-sxml-process (lambda (sxml)
                              (pre-post-order* sxml (append html-rules alist-conv-rules*)))))
  (test "css-link"
        '(link (@ (type "text/css") (href "site.css") (rel "stylesheet")))
        (basic-sxml-process `(css-link "site.css")))

  (test "js-link"
        '(script (@ (type "text/js") (src "site.js")))
        (basic-sxml-process `(js-link "site.js")))

  (test "url without args"
        '(a (@ (href "http://api.call-cc.org/")) "chicken" 1)
        (basic-sxml-process `(url "http://api.call-cc.org/" "chicken" 1)))

  (test "url with args"
        '(a (@ (id "link1") (href "http://api.call-cc.org/")) "chicken")
        (basic-sxml-process `(url (@ (id "link1")) "http://api.call-cc.org/" "chicken")))

  (test "itemize without args"
        '(ul (li "1") (li "2"))
        (basic-sxml-process `(itemize "1" "2")))

  (test "itemize with args"
        '(ul (@ (id "list1")) (li "1") (li "2"))
        (basic-sxml-process `(itemize (@ (id "list1")) "1" "2")))))

(test-group "bootstrap-rules"
  (let ((basic-sxml-process (lambda (sxml)
                              (pre-post-order* sxml (append (bootstrap-rules) html-rules alist-conv-rules*)))))

    (test "row without args"
          '(div (@ (class "row")) "none")
          (basic-sxml-process `(row "none")))

    (test "row with args"
          '(div (@ (id "c1") (class "row")) "none")
          (basic-sxml-process `(row (@ (id "c1")) "none")))

    (test "row with class"
          '(div (@ (id "c1") (class "row float")) "none")
          (basic-sxml-process `(row (@ (id "c1") (class "float")) "none")))

    (test "container without args"
          '(div (@ (class "container")) "none")
          (basic-sxml-process `(container "none")))

    (test "container with args"
          '(div (@ (id "c1") (class "container")) "none")
          (basic-sxml-process `(container (@ (id "c1")) "none")))

    (test "container with class"
          '(div (@ (id "c1") (class "container float")) "none")
          (basic-sxml-process `(container (@ (id "c1") (class "float")) "none")))

    (test "page-header without args"
          '(div (@ (class "page-header")) "none")
          (basic-sxml-process `(page-header "none")))

    (test "page-header with args"
          '(div (@ (id "c1") (class "page-header")) "none")
          (basic-sxml-process `(page-header (@ (id "c1")) "none")))

    (test "page-header with class"
          '(div (@ (id "c1") (class "page-header float")) "none")
          (basic-sxml-process `(page-header (@ (id "c1") (class "float")) "none")))))

(test
  "complete"
  '(html
     (head
       (link (@ (type "text/css") (href "site.css") (rel "stylesheet")))
       (script (@ (type "text/js") (src "site.js"))))
     (body
       (div (@ (class "container"))
            (div (@ (class "row") (id "row1"))
                 (a (@ (href "www.google.com") (class "button")) "google")
                 (a (@ (href "www.google.com")) "google")
                 (ul
                   (li (a (@ (href "http://google.com"))))
                   (li (a (@ (href "https://duckduckgo.com/")) "Duck Duck Go!")))
                 (div 1)
                 (div 2)))))
  (pre-post-order*
    `(html
       (head
         (css-link "site.css")
         (js-link "site.js"))
       (body
         (container
           (row (@ (id "row1"))
                (url (@ (class "button")) "www.google.com" "google")
                (url "www.google.com" "google")
                (itemize
                   (url "http://google.com")
                   (url "https://duckduckgo.com/" "Duck Duck Go!"))
                (div 1)
                (div 2)))))
    (append (bootstrap-rules) html-rules alist-conv-rules*)))

(test-exit)
